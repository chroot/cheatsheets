* freenode

- /server add freenode irc.freenode.net
- /set irc.server.freenode.autoconnect on
- /set irc.server.freenode.nicks <nick>
- /set irc.server.freenode.username <user>
- /set irc.server.freenode.realname <name>
- /set irc.server.freenode.command '/msg nickserv identify xxxxxxx'
- /set irc.server.freenode.autojoin #archlinux,#gentoo,#openbsd,#freebsd,#funtoo,##php,#zsh,#emacs
- /set irc.server.freenode.command_delay 10 :: after command, before autojoin
- /set weechat.bar.buflist.position right
- /connect freenode

------

- /fset :: interactive settings
